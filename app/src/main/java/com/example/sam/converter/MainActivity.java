package com.example.sam.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mm;
    private Button convertButton;
    private TextView inches;
    private Button exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mm = (EditText)findViewById(R.id.millimeters);
        inches = (TextView)findViewById(R.id.inches);
        convertButton = (Button)findViewById(R.id.convert);
        exit = (Button)findViewById(R.id.exit);

        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Double value = Double.valueOf(mm.getText().toString())/25.4;
                inches.setText(value.toString());

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}


